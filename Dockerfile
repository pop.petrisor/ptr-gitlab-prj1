FROM maven:3-jdk-8-alpine

RUN mkdir -p /var/test/newfolder
RUN chown pop /var/test/newfolder
USER pop
WORKDIR /var/test/newfolder
	
COPY . /var/test/newfolder
RUN mvn package

ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
